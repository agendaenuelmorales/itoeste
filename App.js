import React from 'react'
import { Provider } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import { SafeAreaView, View } from 'react-native'
import Store from './src/redux/config'
import StackNavigation from './src/config/routes'

const App = () => {
  const initStore = Store()

  return (
    <Provider store={initStore}>
      <NavigationContainer>
        <StackNavigation />
        <SafeAreaView>
          <View>
            {/* Screen configurations */}
          </View>
        </SafeAreaView>
      </NavigationContainer>
    </Provider>
  )
}


export default App
