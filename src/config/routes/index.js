import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Home from '../../containers/Home'
import { moderateScale } from 'react-native-size-matters'
import { ROUTES_NAME } from './constants'

const StackNavigation = () => {
  const RootStack = createStackNavigator()
  const MainStack = createStackNavigator()
  const { HOME_ROUTE } = ROUTES_NAME

  const MainStackScreen = () => {
    return (
      <MainStack.Navigator>
        <MainStack.Screen
          name={HOME_ROUTE}
          component={Home}
          options={{
            title: 'Home',
            headerTitleStyle: {
              color: '#222',
              fontSize: moderateScale(14)
            },
            headerTitleAlign: 'center',
            headerStyle: {
              backgroundColor: '#CCFEFF',
              elevation: 0,
              shadowOpacity: 0
            },
            headerBackImage: () => null,
            header: () => null,
            headerLeft: () => null
          }}
        />
      </MainStack.Navigator>
    )
  }

  const RootStackScreen = () => {
    return (
      <RootStack.Navigator headerMode="none">
        <RootStack.Screen name="main" component={MainStackScreen} />
      </RootStack.Navigator>
    )
  }

  return RootStackScreen()
}

export default StackNavigation
