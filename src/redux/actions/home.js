import { HOME_TYPES } from '../types'
import HomeService from '../../services/home'

const { SET_TITLE, SET_TITLE_SUCCESS, SET_TITLE_FAILURE } = HOME_TYPES

const actionCreators = {
  setTitle: newTitle => async dispatch => {
    dispatch({ type: SET_TITLE })

    try {
      const response = await HomeService.anything()

      if (response.status === 200) {
        dispatch({ type: SET_TITLE_SUCCESS, payload: newTitle })
      }
    } catch (error) {
      dispatch({ type: SET_TITLE_FAILURE, payload: error })
    }
  }
}

export default actionCreators
