import produce from 'immer'
import { HOME_TYPES } from '../types'
import { HOME_INITIAL_STATE } from '../store'

const { SET_TITLE, SET_TITLE_SUCCESS, SET_TITLE_FAILURE } = HOME_TYPES

export default (state = HOME_INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case SET_TITLE:
      return produce(state, draftState => {
        draftState.loading = true
      })
    case SET_TITLE_SUCCESS:
      return produce(state, draftState => {
        draftState.title = payload
        draftState.loading = false
      })
    case SET_TITLE_FAILURE:
      return produce(state, draftState => {
        draftState.loading = false
        draftState.error = payload
      })
    default:
      return state
  }
}
