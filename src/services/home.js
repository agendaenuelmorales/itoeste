const HomeService = {
  anything: async () => {
    const response = {
      status: 400,
      message: null
    }
    let responseApi

    try {
      await new Promise(resolve => setTimeout(resolve, 3000))
      responseApi = true
    } catch (error) {
      console.error(error)
    }

    if (responseApi) {
      response.status = 200
    } else {
      response.status = 204
      response.message = 'No pudimos obtener los datos.'
    }

    return response
  }
}

export default HomeService
