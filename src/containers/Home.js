import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Platform, StyleSheet, StatusBar, View } from 'react-native'
import { object } from 'prop-types'
import homeActions from '../redux/actions/home'
import Title from '../components/Title'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF0'
  }
})

const Home = ({ navigation }) => {
  const dispatch = useDispatch()
  const { loading, title } = useSelector(state => state.home)

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (Platform.OS !== 'ios') {
        StatusBar.setBackgroundColor('#FF0')
        StatusBar.setBarStyle('dark-content')
      }
      setTimeout(() => {
        dispatch(homeActions.setTitle('¡Carga exitosa!'))
      }, 2000)
    })

    return () => {
      return unsubscribe
    }
  })

  return (
    <View style={styles.container}>
      <Title>{loading ? 'Cargando...' : title}</Title>
    </View>
  )
}

Home.propTypes = {
  navigation: object
}

export default Home
