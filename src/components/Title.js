import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

const styles = StyleSheet.create({
  title: {
    color: '#222',
    fontSize: moderateScale(21),
    fontWeight: 'bold'
  }
})

const Title = ({ children }) => (
  <Text style={styles.title}>{children}</Text>
)

export default Title